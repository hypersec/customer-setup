# hypersec-setup-beats

Automated customer executed beats installation

## Command Line usage

hypersec-setup-beats options  
Parameters ARE case sensitive

## Parameters required for automated installation

>`-customerName` Your Hypersec customer e.g. mycustomername  
>`-cloudInstance` The cloud instance used e.g. privau2,privau1,pubcloud  
>`-customerInstance` The customer instance number if there are more than one  e.g. 1  
>`-collectorInstance` The collector instance number you will send data to e.g. 1  
>`-collectorType` Send data to a local (on prem) or cloud collector e.g. local or cloud  
>`-beatsType` The type of beats installation e.g. winlogbeat, filebeat, metricbeat, heartbeat, auditbeat  

## Parameters required for automated cloud collector installation

>`-cloudCode` Your HyperSec cloud code supplied to retrieve client certificates  

## Optional Parameters for automated installation

>`-beatsVersion` (optional) The beats version you explicity require e.g. 7.10.2  
>`-proxy` (optional) The proxy you wish to use. The system proxy is used by default.  

## Example 1 - Install Metricbeat for a local collector for "Gov Agency" on privau2

`./hypersec-setup-beats.ps1 -proxy none -customerName govagency -cloudInstance privau2 -customerInstance 1 -collectorInstance 1 -collectorType local -beatsType metricbeat`

## Example 2 - Install Winlogbeat for a cloud collector for "Commercial Co" on privau1

`./hypersec-setup-beats.ps1 -proxy none -customerName commercialco -cloudInstance privau1 -customerInstance 1 -collectorInstance 1 -collectorType cloud -cloudCode 12345678 -beatsType winlogbeat`
