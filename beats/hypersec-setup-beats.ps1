#Requires -RunAsAdministrator

<#
.SYNOPSIS
HyperSec automated installation for customer deployed and maintained Beats for Windows.

.DESCRIPTION
For automated installation the following must be supplied
customerName
cloudInstance
customerInstance
collectorInstance
collectorType
cloudCode    (if using a cloud collector)
beatsType
proxy

.\hypersec-setup-beats.ps1 -customerName mycustomername -cloudInstance 1 -customerInstance 1 -collectorInstance 1 -beatsType winlogbeat -proxy none

.EXAMPLE
PS> .\hypersec-setup-beats.ps1 -customerName mycustomername -cloudInstance 1 -customerInstance 1 -collectorInstance 1 -beatsType winlogbeat -proxy none -collectorType local

.LINK
Elasticsearch license https://www.elastic.co/blog/elastic-license-v2
#>

#
# v4.3.1    2021-10-01      Proxy / internet issue detection at startup w/fail
# v4.3      2021-10-01      Direct download 
#                           Cloud code
# v4.2      2021-09-24      Preferred beats version fix
#                           Add collectorType to the cmd .DESCRIPTION
# v4.1      2021-09-20      Verbose and clear repository file names
#                           Update all beats yml for 3 root cas
#                           Add filebeat
#                           Add README.md
# v4.0      2021-09-17      Added cloud collectors
#                           Added client certificate option
#                           ALL .Net member usage removed to ensure secure group policy / environment friendly deploys
# v3.10     2021-07-22      Removed Netlogon dependency
# v3.9      2021-07-21      Path creation fixes
#                           Proxy cache fix
# v3.8      2021-07-01      Command line executable parameters for automated deployment
#                           Cache setup answers
#                           Multiple minor fixes
# v3.7      2021-07-01      Force ca certs to ensure root and intermediate certs are 
#                           updated irrespective of customer host root ca currency                         
# v3.6      2021-06-30      Language constraint friendly version, heartbeat added
# v3.5      2021-06-29      Colour name fixes for older powershell versions
#                           Proxy detection and selection improvements
# v3.4      2021-06-28      Moved under C:\Program Files to be whitelist friendly
#                           Inherit permissions instead of setting new perms
#                           Proxy collected from current user
# v3.3      2021-06-25      Proxy and language security fixes
# v3.2      2021-06-24      allca -> rootca fix
#                           ssl_certificate_authorities -> ssl.certificate_authorities fix
# v3.1      2021-06-10      DNS resolution fix
# v3.0      2021-05-17      Updated out of integration testing
#

#
# Configurations and automated installation
# Copyright 2021 by HyperSec
# All rights reserved
#

param (
    [string]$beatsVersion = "",
    [string]$customerName = "",
    [string]$cloudInstance = "",
    [string]$customerInstance = "",
    [string]$collectorInstance = "",
    [string]$collectorType = "",
    [string]$cloudCode = "",
    [string]$beatsType = "",
    [string]$proxy = "",
    [boolean] $debug = $false, 
    [boolean] $help 
 )

$customerName = $customerName.ToLower()
$beatsType = $beatsType.ToLower()
$proxy = $proxy.ToLower()
$collectorType  = $collectorType.ToLower()

$currentVersion = "4.3"
$default_num = "1"
$default_cloud = "privau2"
$beatsRoot="C:\Program Files\Beats"

$preferredVersionUrl = "https://hypersec.io/customer-setup/preferred-beats-version.txt"
Write-Host -f Gray "HyperSec automated installation for customer deployed and maintained Beats for Windows v$currentVersion"
Write-Host -f Gray "Elasticsearch license https://www.elastic.co/blog/elastic-license-v2"
Write-Host

function Get-File-Http ($sourceUrl, $destinationFile, $fileDescription) {
    try {
        if ($debug ){
            Write-Host -f Green "Retrieving url $sourceUrl"
        }
        if ( $proxy -ne "none" ) {            
            Invoke-WebRequest "$sourceUrl" -Proxy "$proxy" -ProxyUseDefaultCredentials -OutFile "$destinationFile"
        }
        else {
            Invoke-WebRequest "$sourceUrl" -OutFile "$destinationFile"
        }
        if ($fileDescription -eq "") {        
            $fileDescription = $destinationFile.SubString($location.LastIndexOf('/') + 1)
        }
        Write-Host -f Green "Succesfully retrieved $fileDescription"
    }
    catch {
        Write-Host -f Red "ERROR: Unable to retrieve $fileDescription"
        Write-Host -f Red $_
        exit 1
    }
}

function isProxyAddress($address) {
	$uri = $address -as [System.URI]
	$null -ne $uri.AbsoluteURI -and $uri.Scheme -match '[http]'
}

function readInput ($varString, $varName, $regexString, $defaultString = "", $isPassword = $false ) {
    $varFileName = $varName -replace " ", "_" 
    $varFileName = "hypersec_$varFileName" 
    $answerCacheFile = "$beatsRoot\setup\$varFileName"
    if ( Test-Path "$answerCacheFile" -PathType Leaf ){
        $defaultString = Get-Content "$answerCacheFile"
        $defaultString = $defaultString.Trim()
    }
    while (!($varString -Match $regexString)) {
        if ( $varString -ne "" ) {
            Write-Host -f DarkYellow "Incorrect value: $varString" 
        }
        $defaultInsert = ""
        if ( $defaultString -ne "") {
            $defaultInsert = "[$defaultString]"
        }
        if ( $isPassword )
        {
            $varString = Read-host "Please enter the $varName $defaultInsert"
        }
        else
        {
            $varString = Read-host "Please enter the $varName $defaultInsert"
        }
        if ( ( $varString -eq "" ) -and ( $defaultString -ne "" ) ) {
            $varString = $defaultString
        }
    } 
    if ( -Not $isPassword ) {
        try {
            $varString | Out-File "$answerCacheFile"
        }
        catch {
            Write-Host -f Red "ERROR: Unable to write to  $answerCacheFile"
            Write-Host -f Red $_
            exit 1
        }
    }
    return $varString
}

if ($help -eq $true)
{
    write-host "The HyperSec assist script for Beats is executed in either interactive or automated installation mode"
}


try {
    $null = New-Item -ItemType Directory -Force "$beatsRoot\setup"
    $null = New-Item -ItemType Directory -Force "$beatsRoot\pki"
    Write-Host -f Green "Succesfully created initial Beats directory structure under $beatsRoot"
} 
catch {
    Write-Host -f Red "ERROR: Unable to create initial Beats directory structure under $beatsRoot"
    Write-Host -f Red $_
    exit 1
}

if ( ( ! ( isProxyAddress( $proxy ) ) ) -and ( $proxy -ne "none" ) ) {
    $answerCacheFile = "$beatsRoot\setup\hypersec_customer_proxy"
    if ( Test-Path "$answerCacheFile" -PathType Leaf ){
       $proxy  = Get-Content "$answerCacheFile"
       $proxy  = $proxy.Trim()
    }
    $qProxy = $false
    Do {
        $proxy = Read-host "Please enter the proxy to use if present (ENTER for default/none)"

        if ( "$proxy" -eq "" ) {
            $proxy = "none"
            $qProxy = $true
        }
        elseif ( isProxyAddress( $proxy ) ) {
            $qProxy = $true
        }
        else
        {
            Write-Output "Proxy format must be in http://host.domain:port format (https not permissible)"
        }
    } until ($qProxy -eq $true)
    try {
        $proxy | Out-File "$answerCacheFile"
    }
    catch {
        Write-Host -f Red "ERROR: Unable to write to  $answerCacheFile"
        Write-Host -f Red $_
        exit 1
    }
}

$preferredBeatsVersionFile = "$beatsRoot\setup\hypersec_preferred_beats_version"
if ( -not($beatsVersion -match "^(\d+\.)?(\d+\.)?(\*|\d+)$") ){    
    Get-File-Http "$preferredVersionUrl" "$preferredBeatsVersionFile" "HyperSec preferred beats version"
    $beatsVersion = Get-Content "$preferredBeatsVersionFile"
    $beatsVersion = $beatsVersion.Trim()
    if ( -not($beatsVersion -match "^(\d+\.)?(\d+\.)?(\*|\d+)$") ){
        Write-Host -f Red "Unable to obtain preferred beats (internet / proxy issue ?)"
        Write-Host -f Red $beatsVersion 
        exit 1
    }
}
else {
    Write-Host -f Green "Beats version set to $beatsVersion"
}
$beatsVersion | Out-File "$preferredBeatsVersionFile"

$customerName  = readInput $customerName "customer name" "^[a-zA-Z]+$" ""
$cloudInstance = readInput $cloudInstance "cloud instance" "^[a-zA-Z0-9]+$" $default_cloud
$customerInstance = readInput $customerInstance "customer instance" "^[0-9]+$" $default_num
$collectorInstance = readInput $collectorInstance "collector instance" "^[0-9]+$" $default_num
$collectorType = readInput $collectorType "collector type " "^(local|cloud)" "local"
if ( $collectorType -eq "cloud" ) {    
    $cloudCode = readInput $cloudCode "cloud code" "^(?!\s*$).+" "" 
}

$beatsTypes = "winlogbeat", "metricbeat", "auditbeat", "heartbeat", "filebeat", "[cancel]"
if ( ! ( $beatsTypes.Contains($beatsType) )) {
    if ( $beatsType -ne "" ) {
        Write-Host -f DarkYellow "Incorrect beats type supplied: $beatsType"
    }
    Write-Host "Select beats type to install:"
    For ($i=0; $i -lt $beatsTypes.Count; $i++)  {
        Write-Host "$($i+1): $($beatsTypes[$i])"
    }
    [int]$number =0
    while($number -lt 1 -or $number -gt 5) 
    {
      [int]$number = Read-Host "Press the number to select a beats type"
    }
    if ( $number -eq 5 ) {
        Write-Host "Installation Cancelled"
        exit
    }
    $beatsType = $beatsTypes[$number-1]
}
$beatsPath = "$beatsRoot\$($beatsType)"
try {
    $null = New-Item -ItemType Directory -Force "$beatsPath"
    $null = New-Item -ItemType Directory -Force "$beatsPath\data"
    $null = New-Item -ItemType Directory -Force "$beatsPath\log"
    Write-Host -f Green "Succesfully created $beatstype directory structure under $beatsPath"
} 
catch {
    Write-Host -f Red "ERROR: Unable to create $beatstype directory structure under $beatsPath"
    Write-Host -f Red $_
    exit 1
}

Write-Host -f Green "Installing into $beatsRoot\$($beatsType)"

$beatsUrl = "https://artifacts.elastic.co/downloads/beats/$beatsType/$beatsType-$beatsVersion-windows-x86_64.zip"
$BeatsZipFile = "$beatsRoot\setup\$beatsType-$beatsVersion-windows-x86_64.zip"
if (-not(Test-Path -Path "$BeatsZipFile" -PathType Leaf)) {
    Get-File-Http "$beatsUrl" "$BeatsZipFile" "$beatsType-$beatsVersion-windows-x86_64.zip..."
}
else
{
    Write-Host -f Green "Using existing $beatsRoot\setup\$beatsType-$beatsVersion-windows-x86_64.zip"
}

Write-Host -f Green "Extracting Beats archive"
Expand-Archive -Path "$BeatsZipFile" -DestinationPath "$beatsPath" -Force
Copy-Item -Path "$beatsPath\$beatsType-$beatsVersion-windows-x86_64\*" -Destination "$beatsPath" -Force -Recurse
Remove-Item "$beatsPath\$beatsType-$beatsVersion-windows-x86_64" -Recurse -Force

$beatsConfigFile = "$beatsRoot\$beatsType\$beatsType.yml"
if ( Test-Path "$beatsConfigFile" -PathType Leaf ){
    $ts = Get-Date -Format ".yyyy-MM-dd@HH-MM~"
    $backupFile = "$beatsConfigFile$ts"
    if ( ! ( Test-Path "$backupFile " -PathType Leaf ) ){
        Write-Host -f Green "Backing up existing config to $backupFile"
        Copy-Item -Path "$beatsConfigFile" "$backupFile"
    }    
}
if ( $collectorType -eq "cloud" ) {  
    Get-File-Http "https://hypersec.io/customer-setup/cloud-collector-windows-$beatsType.yml" "$beatsConfigFile" "HyperSec integrated $beatsType.yml"
}
else {
    Get-File-Http "https://hypersec.io/customer-setup/windows-$beatsType.yml" "$beatsConfigFile" "HyperSec integrated $beatsType.yml"    
}
If ($beatsType -eq "metricbeat") {
    Get-File-Http "https://hypersec.io/customer-setup/windows-metricbeat-system.yml" "$beatsRoot\metricbeat\modules.d\system.yml" "HyperSec metricbeat-system.yml"
    Get-File-Http "https://hypersec.io/customer-setup/windows-metricbeat-windows.yml" "$beatsRoot\metricbeat\modules.d\windows.yml" "HyperSec metricbeat-windows.yml"
}

Get-File-Http "https://hypersec.io/customer-setup/rootca1.crt" "$beatsRoot\pki\rootca1.crt" "HyperSec root CA certificate 1"
Get-File-Http "https://hypersec.io/customer-setup/rootca2.crt" "$beatsRoot\pki\rootca2.crt" "HyperSec root CA certificate 2"
Get-File-Http "https://hypersec.io/customer-setup/rootca3.crt" "$beatsRoot\pki\rootca3.crt" "HyperSec root CA certificate 3"

Get-File-Http "https://hypersec.io/customer-setup/intca1.crt" "$beatsRoot\pki\intca1.crt" "HyperSec intermediate CA certificate 1"
Get-File-Http "https://hypersec.io/customer-setup/intca2.crt" "$beatsRoot\pki\intca2.crt" "HyperSec intermediate CA certificate 2"
Get-File-Http "https://hypersec.io/customer-setup/intca3.crt" "$beatsRoot\pki\intca3.crt" "HyperSec intermediate CA certificate 3"

if ( $collectorType -eq "cloud" ) {   
    Get-File-Http "https://hypersec.io/customer-setup/$customerName-$cloudCode-hypersec-cloud-collector.crt" "$beatsRoot\pki\hypersec-cloud-collector.crt" "HyperSec $customerName cloud collector client certificate"
    Get-File-Http "https://hypersec.io/customer-setup/$customerName-$cloudCode-hypersec-cloud-collector.key" "$beatsRoot\pki\hypersec-cloud-collector.key" "HyperSec $customerName cloud collector client key"
}

if ( $collectorType -eq "cloud" ) {   
    $collectorHost = "$customerName$customerInstance-cloudcollector$collectorInstance.collectors.$cloudInstance.hypersec.io:5044"
}
else {
    $collectorHost = "$customerName$customerInstance-collector$collectorInstance.collectors.$cloudInstance.hypersec.io:5044" 
}

((Get-Content -path "$beatsPath\$beatsType.yml" -Raw) -replace "<collector host>","$collectorHost") | Set-Content -Path "$beatsPath\$beatsType.yml"
Write-Host -f Green "Succesfully configured $beatsType"

$serviceDescription = $beatsType.substring(0,1).toupper() + $beatsType.substring(1).tolower()
if (Get-Service $serviceDescription -ErrorAction SilentlyContinue) {
    Write-Host -f Green "Removing existing $beatsType service"
    $service = Get-WmiObject -Class Win32_Service -Filter "name='$serviceDescription'"
    $service.StopService() | out-null
    Start-Sleep -s 1
    $service.delete() | out-null
}

$params = @{
    Name = "$serviceDescription"
    BinaryPathName = "`"$beatsPath/$beatsType.exe`" --environment=windows_service -c `"$beatsPath/$beatsType.yml`" --path.home `"$beatsPath`" --path.data `"$beatsPath/data`" --path.logs `"$beatsPath/logs`" -E logging.files.redirect_stderr=true"
    DisplayName = "$serviceDescription"
    StartupType = "Automatic"
    Description = "$serviceDescription"
}
Try {
    New-Service @params | out-null
    Write-Host -f Green "Succesfully installed the $beatsType service"
}
catch  {
    Write-Host -f Red "ERROR: Unable to install the $beatsType service" 
    Write-Host -f Red $_
    exit 1
}
Try {
    Start-Process -FilePath sc.exe -ArgumentList "config $serviceDescription start= delayed-auto"
}
Catch { 
    Write-Host -f Red "ERROR: Unable to set the $beatsType service to delayed start."
    Write-Host -f Red $_
    exit 1
}

Write-Host -f Green "Succesfully installed $beatsType with HyperSec integration"
